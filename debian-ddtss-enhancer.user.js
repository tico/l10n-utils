// ==UserScript==
// @name                debian-ddtss-enhancer
// @name:pt-BR          debian-ddtss-enhancer
// @author              Thiago Pezzo (tico)
// @namespace           https://salsa.debian.org/tico/l10n-utils
// @version             0.3
// @icon                https://www.debian.org/favicon.ico
// @license             GPL-3.0-or-later
// @description         Automates text and translation filling in DDTSS (Debian DDTP's web interface).
// @description:pt-BR   Automatiza preenchimento de texto e traduções no DDTSS (interface web do Debian DDTP).
// @match               *://ddtp.debian.org/ddtss/index.cgi/*/translate/*
// @match               *://ddtp.debian.org/ddtss/index.cgi/*/forreview/*
// ==/UserScript==

    'use strict';

    // -------------------------------
    // OPÇÕES - altere esta seção
	// -------------------------------
    const NOME = 'ThiagoPezzo'; // nome do(a) tradutor(a) para comentários
    // -------------------------------

    // Define data atual
    const DATA = new Date();
    const MES = (((DATA.getMonth() + 1) < 10) ? '0' : '') + (DATA.getMonth()+1);
    const DIA = ((DATA.getDate() < 10) ? '0' : '') + DATA.getDate();
    const ANO = DATA.getFullYear();
    const HOJE = '' + ANO + MES + DIA;

    // Define padrão de comentário com nome e data
    const URL = window.location.href;
    const TRADUCAO = HOJE + ': ' + NOME + ': tradução.';
    const REVISAO = HOJE + ': ' + NOME + ': revisão.';
    const COMENTARIO = ((URL.includes("forreview")) ? REVISAO : TRADUCAO);

    // Instancia objetos da página
    var txtareaDescCurta = document.getElementsByName("short");
    var txtareaDescLonga = document.getElementsByName("long");
    var txtareaDescComentario = document.getElementsByName("comment");

     // Insere comentário personalizado
    function insereComentario() {
        let NL = ((txtareaDescComentario[0].value == '') ? '' : '\n');
        txtareaDescComentario[0].value += NL + COMENTARIO;
    };

    // Valida opções
    function validaOpcoes() {
        // Se não está na página de tradução ou revisão, sai
        let padraoURL = /^https?:\/\/ddtp\.debian\.org\/ddtss\/index\.cgi\/.+\/translate|forreview\/.+$/;
        if (!padraoURL.test(URL)) {
            return;
        }

       	// Verifica nome do(a) tradutor(a)
        let ERRO = "[debian-ddtss-enhancer] Nome do(a) tradutor(a) vazio - corrija o script.";
        if (NOME === '') {
            alert(ERRO);
            return;
        };
    };


    //    MAIN

    // Foco inicial na descrição curta
    txtareaDescCurta[0].focus();
    // Valida e executa configurações, senão sai
    validaOpcoes();
	// Comentário personalizado com data, nome e tipo de ação
    insereComentario();

    console.log("[debian-ddtss-enhancer] Personalizações configuradas corretamente.");