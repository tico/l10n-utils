# 💬 Utilitários L10N

Alguns utilitários para as interfaces de localização do Debian, criadas no
contexto  da equipe de tradução pt_BR.


## DDTSS com tema escuro e interface mínima

Código CSS (userstyle) para a extensão [Stylus](https://add0n.com/stylus.html) do Firefox,
que altera a interface DDTSS do Debian para tema mais legível e elementos redimensionados.

Arquivo:
`debian-ddtss-dark-theme-and-declutter.css`

Instalação:
 1. Firefox -> Stylus addon -> Manage -> Import
 2. Selecione o arquivo
 3. Salve e habilite o script

## WIP: DDTSS com mais funções

Código Javascript (userscript) para a extensão [TamperMonkey](https://www.tampermonkey.net/)
do Firefox, que inclui mais recursos para a interface, como:

 * preenchimento automático de comentário com data, nome e ação;
 * ...

Arquivo:
`debian-ddtss-enhancer.user.js`

Instalação:
 1. Firefox -> TamperMonkey addon -> Dashboard -> Utilities -> Import from file...
 2. Selecione o arquivo
 3. Salve e habilite o script


## Wiki com tema escuro e interface mínima

Código CSS (userstyle) para a extensão [Stylus](https://add0n.com/stylus.html) do Firefox,
que altera o wiki do Debian para tema mais legível e elementos redimensionados.

Arquivo:
`debian-wiki-dark-theme-and-declutter.css`

Instalação:
 1. Firefox -> Stylus addon -> Manage -> Import
 2. Selecione o arquivo
 3. Salve e habilite o script


## Script para pesquisa de termos

Facilita a procura por termos no vocabulário padrão, também pesquisando em
serviços on-line de tradução.

Arquivo:
`debian-busca-termo.sh`

Uso:
`$ debian-busca-termo.sh <termo-a-ser-procurado.`


# Links

 * [Portal da Equipe Debian de tradução pt_BR](https://wiki.debian.org/Brasil/Traduzir/ "debian-l10n-portuguese")
 * [Vocabulário padrão](https://salsa.debian.org/l10n-br-team/vocabulario "vocabulário-padrão-l10n-portuguese")


# Licença GPL-3+

These programs are free softwares: you can redistribute them and/or modify them under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

These programs are distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with these programs. If not, see <https://www.gnu.org/licenses/>.
