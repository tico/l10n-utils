#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Script    : debian-busca-termo.sh
# Versão    : 0.6
# Descrição : Pesquisa vocabulário padrão debian-l10n-pt_BR e serviços on-line
# Autor     : Thiago Pezzo (tico) <pezzo@protonmail.com>
# URL       : https://salsa.debian.org/tico/l10n-utils
# Data      : 20-04-2021
# Atualizado: 16-02-2022
# Licença   : GPL-3+
# Depende   : translate-shell, fzf, xclip (opcionais)
# Uso       : debian-busca-termo.sh <termo>
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# LICENSE: GPL-3+
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Exibe mensagens de erro.
# Var. global: ---
# Var. local : msgErro, erro{Parametro,Arquivo,FZF,Trans,Xclip}
# Argumentos : erro
# Retorna    : ---
# -----------------------------------------------------------------------------
function exibeErro {
  # Define array associativo
  local -A msgErro
  msgErro[parametro]="$(gettext "Passe um termo para ser pesquisado.")"
  msgErro[arquivo]="$(gettext "Arquivo vocabulario-pt_BR.md não encontrado. Verifique a localização no script.")"
  msgErro[fzf]="$(gettext "Ignorando seleção rápida (fzf não encontrado).")"
  msgErro[translate]="$(gettext "Ignorando pesquisa on-line (translate-shell não encontrado). ")"
  msgErro[xclip]="$(gettext "Ignorando cópia para clipboard (xclip não encontrado).")"
  echo -e "$strNOK ${msgErro[$1]}" >&2
}

# -----------------------------------------------------------------------------
# Busca termo no vocabulário padrão.
# Var. global: listaAchados
# Var. local : saida, cadaTermo
# Argumentos : ---
# Retorna    : ---
# -----------------------------------------------------------------------------
function pesquisaVocab {
  local saida
  saida="$(grep -i "$termo" "$arquivoVocab" || echo '----')"
  echo -e "${strVocab}\\n$saida\\n"
  # Adiciona palavras encontradas à lista
  while read -r cadaTermo; do
      cadaTermo=${cadaTermo#*| }
      cadaTermo=${cadaTermo% |*}
      listaAchados="${listaAchados}${cadaTermo}"$'\\n'
  done <<< "$saida"
}

# -----------------------------------------------------------------------------
# Busca termo no google.
# Var. global: listaAchados
# Var. local : saida
# Argumentos : ---
# Retorna    : ---
# -----------------------------------------------------------------------------
function pesquisaGoogle {
  local saida
  # 'trans -brief': tradução mais relevante
  saida="$(trans -brief "$termo")"
  echo -e "${strGoogle}\\n$saida\\n"
  # Adiciona à lista de seleção
  listaAchados=${listaAchados}${saida}$'\\n'
}

# -----------------------------------------------------------------------------
# Define cores, símbolos e constantes
# Var. global: ---
# Var. local : arquivoVocab, txt{cia,azl,rst,vrm,vrd}
#            : str{OK,NOK,Titulo,Vocab,Google,Copiado}
# Argumentos : ---
# Retorna    : ---
# -----------------------------------------------------------------------------
function initConstantes {
  # Cores
  readonly txtcia="$(tput setaf 6 2>/dev/null || echo '\e[0;36m')" # Ciano
  readonly txtazl="$(tput setaf 4 2>/dev/null || echo '\e[0;34m')" # Azul
  readonly txtrst="$(tput sgr0    2>/dev/null || echo '\e[0m')"    # Restaura texto padrão
  readonly txtvrm="$(tput setaf 1 2>/dev/null || echo '\e[0;31m')" # Vermelho
  readonly txtvrd="$(tput setaf 2 2>/dev/null || echo '\e[0;32m')" # Verde

  # Símbolos
  readonly strOK="${txtvrd}✓${txtrst}"
  readonly strNOK="${txtvrm}✗${txtrst}"

  # Constantes
  readonly arquivoVocab="/tera/debian/repos/vocabulario/vocabulario-pt_BR.md"
  readonly strTitulo="${txtazl}PESQUISA & TRADUÇÃO${txtrst} <debian-l10n-portuguese>"
  readonly strVocab="${txtazl}Vocabulário${txtrst}"
  readonly strGoogle="${txtazl}Google${txtrst}"
  readonly strCopiado="Escolha copiada para o ${txtvrm}clipboard${txtrst}!"
}

# -----------------------------------------------------------------------------
# Verifica dependências e exibe problemas.
# Var. global: ---
# Var. local : temTrans, temFZF, temXclip, temArquivo
# Argumentos : ---
# Retorna    : ---
# -----------------------------------------------------------------------------
function verificaDependencias {
  # Assume arquivo e dependências instaladas
  temTrans=true
  temFZF=true
  temXclip=true
  temArquivo=true
  # Inverte flag em caso de inexistência
  [[ -f "$arquivoVocab" ]]                  || { exibeErro arquivo; temArquivo=false; }
  command -v /usr/bin/trans >/dev/null 2>&1 || { exibeErro translate; temTrans=false; }
  command -v /usr/bin/fzf   >/dev/null 2>&1 || { exibeErro fzf; temFZF=false; }
  command -v /usr/bin/xclip >/dev/null 2>&1 || { exibeErro xclip; temXclip=false; }
  echo
}

# -----------------------------------------------------------------------------
# Recebe lista de resultados e exibe em um menu selecionável.
# Var. global: traducao
# Var. local : ---
# Argumentos : listaAchados (lista de termos achados)
# Retorna    : traducao (termo selecionado)
# -----------------------------------------------------------------------------
function menuEscolhas {
  traducao=$(echo -e "$listaAchados" | sort | uniq |
              smenu -word_delimiters $'\n' -column -level5 .+)
}

# -----------------------------------------------------------------------------
# Inicia variáveis de ambiente e carrega funções do 'gettext' para localização.
# Var. global: TEXTDOMAIN, TEXTDOMAINDIR
# Var. local : ---
# Argumentos : ---
# Retorna    : ---
# -----------------------------------------------------------------------------
function initL10N {
  # Carrega funções gettext; caminho absoluto: /usr/bin/gettext.sh
  [[ ! -f /usr/bin/gettext.sh ]] && {
      echo >&2 "O script '/usr/bin/gettext.sh' não foi encontrado. Saindo."
      exit 1
  }
  . gettext.sh
  # Variáveis de ambiente: catálogos de tradução
  TEXTDOMAINDIR=/usr/share/locale
  TEXTDOMAIN="${0##*/}"
  export TEXTDOMAINDIR
  export TEXTDOMAIN
}



#-----------
#   MAIN
#-----------

# Define cores, símbolos e constantes
initConstantes

# Carrega configurações e funções de localização
initL10N

# Exibe título
echo -e "$strTitulo"

# Flags para definir ou ignorar certas seções
verificaDependencias

# Enviou um termo?
[[ $# -lt 1 ]] && { exibeErro parametro; exit 1; }

# Inicializa variáveis
termo="$1"
listaAchados=""
traducao=""

# Pesquisas
[[ $temArquivo == true ]] && pesquisaVocab  "$termo"
[[ $temTrans == true ]]   && pesquisaGoogle "$termo"

# Remove linha em branco
listaAchados=$(echo -e "$listaAchados" | sed '/^$/d')

# Menu de escolha dos resultados se mais de uma opção
if (( $(echo -e "$listaAchados" | wc --lines) > 1 )); then
    # Atribui seleção para $traducao
    menuEscolhas "$listaAchados"
else
    traducao="$listaAchados"
fi

# Aviso de cópia para clipboard
[[ $temXclip == true ]] && {
    xclip -in -select clipboard >/dev/null 2>&1 <<< "$traducao"
    echo -e "$strCopiado"
}

# Sai com sucesso
exit 0
