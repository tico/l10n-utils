#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Script    : debian-l10n-report.sh
# Versão    : 0.1
# Descrição : Consolida informações sobre as frentes de tradução em pt_BR do Debian
# Autor     : Thiago Pezzo (tico) <pezzo@protonmail.com>
# URL       : https://salsa.debian.org/tico/l10n-utils
# Data      : 05-02-2023
# Atualizado:
# Licença   : GPL-3+
# Depende   : lynx
# Uso       : debian-l10n-report.sh
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# LICENSE: GPL-3+
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# <descrição da função>
# Var. global:
# Var. local :
# Argumentos :
# Retorna    :
# -----------------------------------------------------------------------------
# Verifica dependências
function verificaDependencias {
    command -v lynx >/dev/null 2>&1 ||
    { msg "O programa lynx não está instalado. Saindo...";
      exit 1; }
}

# -----------------------------------------------------------------------------
# <descrição da função>
# Var. global:
# Var. local :
# Argumentos :
# Retorna    :
# -----------------------------------------------------------------------------
function relatorioDDTP {

    readonly urlDDTSS="https://ddtp.debian.org/ddtss/index.cgi/pt_BR"
    readonly urlDDTP="https://ddtp.debian.org/stats/stats-sid.html"

    revisoesPendentes=$(lynx -dump "$urlDDTSS"    |
        sed -E '/Pending review/,/About you/!d'   |
        sed '$d' | sed '$d' | sed '1,2d' | wc -l)

    statsDDTP=$(lynx -dump "$urlDDTP")
    read lixo lixo lixo required required2 important important2 standard standard2 \
         optional optional2 extra extra2 popcon500 popcon5002 popconrank popconrank2  \
         <<< "$(grep -A1 'pt_BR' <<< "$statsDDTP" | tr -s ' ' | tr -d $'\n')"

    read requiredTotal importantTotal standardTotal optionalTotal \
         extraTotal popcon500Total popconrankTotal                \
         <<< "$(grep 'Package count' <<< "$statsDDTP"             |
                tr -d '[:alpha:]' | tr -s ' ' | tr -d $'\n')"

    echo "
    DDTP - DESCRIÇÕES DE PACOTES
    https://ddtp.debian.org/ddtss/index.cgi/pt_BR
    https://ddtp.debian.org/stats/stats-bullseye.html
    -------------------------------------------------
    Revisões atualmente pendentes: $revisoesPendentes
    Required  : $required $required2 / $requiredTotal
    Important : $important $important2 / $importantTotal
    Standard  : $standard $standard2 / $standardTotal
    Optional  : $optional $optional2 / $optionalTotal
    Extra     : $extra $extra2 / $extraTotal
    Popcon500 : $popcon500 $popcon5002 / $popcon500Total
    Popconrank: $popconrank $popconrank2 / $popconrankTotal"
}

# -----------------------------------------------------------------------------
# <descrição da função>
# Var. global:
# Var. local :
# Argumentos :
# Retorna    :
# -----------------------------------------------------------------------------
function relatorioSiteWeb {
    readonly urlSITE='https://www.debian.org/devel/website/stats/pt.en'

    statsSite=$(lynx -dump "$urlSITE" | grep -A2 'Translated Up to date' |
                  sed '1d' | tr -d '\n' | sed 's/files//g' | tr -s ' ' )
    read lixo lixo uptodate uptodate2 outdated outdated2 lixo <<< "$statsSite"

    echo "
    SITE WEB
    https://www.debian.org/devel/website/stats/pt.en
    ------------------------------------------------
    Páginas atualizadas   : $uptodate $uptodate2
    Páginas desatualizadas: $outdated $outdated2"
}


# -----------------------------------------------------------------------------
# <descrição da função>
# Var. global:
# Var. local :
# Argumentos :
# Retorna    :
# -----------------------------------------------------------------------------
function relatorioCoordenacao {
    readonly urlCOORD='https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html'

    statsCoordenacao=$(lynx -dump "$urlCOORD")
    itt=$(sed -E '/^\[.+itt/,/^\[.+itr/!d'  <<< "$statsCoordenacao" | sed '1d' | grep itt | wc -l)
    itr=$(sed -E '/^\[.+itr/,/^\[.+rfr/!d'  <<< "$statsCoordenacao" | sed '1d' | grep itr | wc -l)
    rfr=$(sed -E '/^\[.+rfr/,/^\[.+lcfc/!d' <<< "$statsCoordenacao" | sed '1d' | grep rfr | wc -l)

    echo "
    COORDENAÇÃO DA LISTA DE DISCUSSÃO
    https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html
    -------------------------------------------------------------------
    Traduções em ITT: $itt
    Traduções em ITR: $itr
    Traduções em RFR: $rfr"
}


# -----------------------------------------------------------------------------
# <descrição da função>
# Var. global:
# Var. local :
# Argumentos :
# Retorna    :
# -----------------------------------------------------------------------------
function relatorioHandbook {
    # Baixa a página e recorta duas seções
    urlHANDBOOK="https://hosted.weblate.org/languages/pt_BR/debian-handbook/"
    pagina=$(lynx -dump "$urlHANDBOOK" | sed -E '/^Quick numbers/,/^   New translation/!d')

    # Estatísticas da seção "Quick numbers"
    statsHandbook=$(sed -E '/^Quick numbers/,/^   New translation/!d' <<< "$pagina" | head)
    translated=$(  grep -B1 Translated   <<< "$statsHandbook" | sed -E '/[0-9]{1,3}%/!d' | tr -d ' ')
    contributors=$(grep -B1 Contributors <<< "$statsHandbook" | sed -E '/[0-9]{1,3}/!d'  | tr -d ' ')

    # Estatísticas da seção "Trends of last 30 days"
    statsHandbook=$(sed -E '/^Quick numbers/,/^   New translation/!d' <<< "$pagina" | tail)
    translatedTrend=$(  grep -B1 Translated   <<< "$statsHandbook" | sed -E '/[0-9]{1,3}%/!d' | tr -d ' ')
    contributorsTrend=$(grep -B1 Contributors <<< "$statsHandbook" | sed -E '/.+[0-9]{1,3}%/!d'  | tr -d ' ')

    echo "
    MANUAL DO(A) ADMINISTRADOR(A) DEBIAN
    https://hosted.weblate.org/languages/pt_BR/debian-handbook/
    -----------------------------------------------------------
    Texto traduzido  : $translated ($translatedTrend nos últimos 30 dias)
    Contribuidores/as: $contributors ($contributorsTrend nos últimos 30 dias)"
}


function relatorioPODebConf {
    urlPODebConfStatus='https://www.debian.org/international/l10n/po-debconf/rank'

    stringsTraduzidas=$(lynx -dump "$urlPODebConfStatus" |
        grep -E '\]pt_BR' | grep -oE '[0-9]{1,5} \([0-9]{1,3}%)')

    echo "
    MODELOS DEBCONF
    https://www.debian.org/international/l10n/po-debconf/rank
    ---------------------------------------------------------
    Strings traduzidas: $stringsTraduzidas"
}


function relatorioWiki {
    urlWiki="https://wiki.debian.org/HelpOnSearching?action=fullsearch&context=180&value=language%3Apt-br+case%3AToDo+case%3AFixMe&fullsearch=Text"
    paginasWiki=$(lynx -dump "$urlWiki" | grep -Eo "of [0-9]{1,3} results" | tr -dc [:digit:])

    urlWikiRevisao='https://wiki.debian.org/Brasil/Traduzir/Wiki/Status?action=raw'
    paginaStatus=$(lynx -dump "$urlWikiRevisao" | grep '|| \[\[pt_BR')
    revisoesTotal=$(wc -l <<< "$paginaStatus")
    revisoesOK=$(grep -E '\|\| Sim \|\| .+ \|\| Sim \|\|' <<< "$paginaStatus" | wc -l)

    echo "
    WIKI
    https://wiki.debian.org/
    https://wiki.debian.org/Brasil/Traduzir/Wiki/Status
    ---------------------------------------------------
    Páginas: $paginasWiki (marcadas com ToDo ou FixMe)
    Páginas: $((revisoesTotal - revisoesOK)) (precisam de revisão)"
}


function relatorioDI {
    urlDI="https://d-i.debian.org/l10n-stats/translation-status.html"

    paginaDI=$(lynx -dump $urlDI | grep pt_BR | sed '4,6d' | tr -d $'\n' | tr -s ' ')
    read diLevel1 lixo diLevel2 lixo diLevel3 lixo <<< "$paginaDI"

    echo "
    INSTALADOR DEBIAN
    https://d-i.debian.org/l10n-stats/translation-status.html
    ---------------------------------------------------------
    Level 1: $diLevel1
    Level 2: $diLevel2
    Level 3: $diLevel3
    "
}


# MAIN

hoje=$(date -u +%Y-%m-%d)
echo "Relatório sobre as frentes de tradução Debian pt_BR em $hoje"
verificaDependencias
relatorioCoordenacao
relatorioSiteWeb
relatorioDDTP
relatorioHandbook
relatorioPODebConf
relatorioWiki
relatorioDI

